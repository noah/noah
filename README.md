## About me

<!--
[**Consider embedding a video of you working or being interviewed, along with 5-10 bullet points outlining interesting tidbits about you and your interests. Focus these on non-work attributes. Also, add detail on what GitLab values and sub-values resonate most with you. This enables ice-breakers to occur ahead of meetings.**]
-->

- It has been a career-long goal for me to work at GitLab ever since using GitLab for the first time 
- Prior to working at Gitlab I worked as a DevOps engineer for 5 years mainly focusing on Cloud Infrastructure using AWS and GCP
- This is my first experience being officially part of a sales team, and I love it. I feel it compliments my deep interest in performance arts
- I have a passionate interest in Hip Hop, Jazz and Funk music. Khruangbin is my favourite band
- I am born and raised in Toronto, Canada.


## My working style

<!-- Add 5-10 bullets on how you prefer to work, interact with others, and learn. You may optionally include intel from Strength Deployment Inventory, Myers—Briggs, etc. This is most effective when you are precise and specific about your norms, assumptions, and expectations. If you are unsure what would be helpful here, ask your colleagues for their input. You may learn something from listening to outside perspective! -->

- I bias towards action. I'm happy to iterate and learn the hard way
- I am a visual learner and enjoy visual cues to get the point across
- I take notes on every meeting I have. I'm very prone to forgetting so this is how I keep up
- I like having an agenda for my meetings sprinkled with light banter and jokes
- I am a stickler for systems and processes. I develop good habits and routines for even the most straight forward task.

## What I assume about others

<!-- Add 5-10 bullets on the assumptions you typically hold when working with others. Strive to be as open with these as possible, so others understand your perspective when engaging with you on projects. Remember, the honesty put forth in these answers enables others to be more understanding and empathetic. -->

- I assume others to have a sense of humour and laugh atleast once in a while
- I assume positive intent at all times. If something comes across negatively, I typically clarify with them
- I assume others will communicate what they are thinking transparently
- I assume that everyone does their due dilligence before meeting
- I assume everyone is respectful of each others time during business hours
